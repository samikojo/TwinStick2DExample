using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GA.TwinStick.Generated;
using System.Linq;

namespace GA.TwinStick
{
	public class PlayerController : MonoBehaviour
	{
		[SerializeField] private float _movementSpeed = 5f;

		private Controls _controls;

		private void Awake()
		{
			_controls = new Controls();
		}

		private void OnEnable()
		{
			_controls.Game.Enable();
		}

		private void OnDisable()
		{
			_controls.Game.Disable();
		}

		private void Update()
		{
			Vector2 moveInput = _controls.Game.Move.ReadValue<Vector2>();
			Move(moveInput);

			Vector2 lookInput = _controls.Game.Look.ReadValue<Vector2>();
			if (_controls.Game.Look.activeControl != null &&
				_controls.Game.Look.activeControl.device.name == "Mouse")
			{
				// Mouse
				Debug.Log("Mouse");
				MouseLook(lookInput);
			}
			else
			{
				// Gamepad
				Debug.Log("Gamepad");
				GamepadLook(lookInput);
			}
		}

		private void Move(Vector2 moveInput)
		{
			Vector2 movement = moveInput * (_movementSpeed * Time.deltaTime);
			transform.Translate(movement, Space.World);
		}

		private void MouseLook(Vector2 mousePosition)
		{
			// LookInput contains mouse position in screen coordinates
				Vector3 mousePoint = Camera.main.ScreenToWorldPoint(mousePosition);
				mousePoint.z = 0;
				transform.up = mousePoint - transform.position;
		}

		private void GamepadLook(Vector2 lookInput)
		{
			Vector3 upDirection = transform.position + (Vector3)lookInput;
			transform.up = upDirection - transform.position;
		}
	}
}